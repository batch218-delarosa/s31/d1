// Use the "require" directive/command to lode the Node.js modules
//A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (devices/browser) and server (nodeJS/expressJS application) communicate by exchanging individual messages (requests/responses);
// Request - the message sent by the client
// Response - the message sent by the server as response


let http = require('http');

// The "http module" has a createServer() method that accepts a function as argument and allows for a creation of a server.

// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// createServer() is a method of the http object responsible for creating a server using Node.js

http.createServer((req, res) => {
	// Use the writeHead() method to:
		// Set a status code for the response. (200 means okay)
			// HTTP Response status codes https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

/*	Informational responses (100 – 199)
	Successful responses (200 – 299)
	Redirection messages (300 – 399)
	Client error responses (400 – 499)
	Server error responses (500 – 599)*/

		// Set the content-type of the response
	res.writeHead(200, {"Content-Type" : 'text/plain'});

	res.end("Hello World");

}).listen(3000); // A port is a virtual point where network connections start and end

// Each port is associated with specific process or services
// The server will be assigned to port 3000 via the listen() method
	// Where the server will listen to any request that are sent to it and will also send the response via this port.

// 3000,4000,5000,8000 - usually used for web development

console.log("Server is running at localhost: 3000");
console.log("You may now use the application");

// We will access the server in the browser using the localhost:3000


// Command to run our web application (to be used in our gitbash):

/*
	1. Install nodemon packages:
	- npm install -g nodemon
	- npm i -g nodemon

	2. Once nodemon packages are already installed/downloaded, you may not install the nodemon again
	- nodemon (file to access)

*/
