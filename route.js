const http = require('http');


// Creates a variable 'port' to store the port number
// 3000, 4000, 5000, 8000 - usually used for web development
const port = 3000;

const server = http.createServer((req, res) => {
	// We will create two endpoint routes for "/greeting" and "/homepage" and will return a resonse upon accessing.
	console.log(req.url);
	// The "url" property refers to the url or the link in the browser (endpoint)


	if(req.url == "/greeting") {
		res.writeHead(200, {"Content-Type": "text/html"});
		res.end(`Hello again
				<a href='/greeting'><h1>Greet Me</h1></a>
				<a href='/'><h1>Landing Page</h1></a>
				<a href='/homepage'><h1>Home Page</h1></a>
			`);
	} else if(req.url == "/homepage") {
		res.writeHead(200, {"Content-Type": "text/html"});
		res.end(`You are now in my homepage!
						<a href='/greeting'><h1>Greet Me</h1></a>
						<a href='/'><h1>Landing Page</h1></a>
						<a href='/homepage'><h1>Home Page</h1></a>
			`);
	} else if (req.url == "/") {
		res.writeHead(200, {"Content-Type": "text/html"});
		res.end(`This is the landing page! 
			<a href='/greeting'><h1>Greet Me</h1></a>
			<a href='/'><h1>Landing Page</h1></a>
			<a href='/homepage'><h1>Home Page</h1></a>

			`);
	} else {
		res.writeHead(404, {"Content-Type": "text/text"});
		res.end("Page not available");
	}

});


server.listen(port);

console.log(`Listening at ${port}`);